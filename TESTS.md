# Testing
## Explanation
This is my testing document, it will show all if tests I have run, whether manual.\
Manual testing will follow the following format:
- What I'm testing
  - The date (dd/mm/yyyy) (Multiple dates if multiple tests have been run and have the same outcome)
  - Amount of tests, (How many passes and fails)
  - Expected output, or response
    - What happened (multiple for different tests)
      - Notes (if any)

## Tests
### GUI main tests
- GUI size
  - 12/09/2023
  - 2 tests (1 passed, 1 failed)
  - The GUI should show at the correct size of 800x600
    - The GUI did not show at the correct size, it was too small
      - Java includes the size of the title bar and borders to the size of the window, so I had to make up for that
    - The GUI was the correct size
      - N/A
- Various text panel colours
  - 14/09/2023
  - 3 tests (1 passed, 2 failed)
  - The text panel should be dark, text light, and input should be lighter than the background, the button to the right should be similar to the text box
    - Outcome was mostly as expected, however the borders were still default. The button was not changed
      - I forgot about the borders and button
    - Outcome was mostly as expected, button border was still default
      - I forgot about the button border
    - Outcome was as expected
      - I need to stop forgetting to take things into account
- Main window body colour
  - 15/09/2023
  - 4 tests (1 passed, 3 failed)
  - The "body" of the window should be gray
    - (3 times) No change
      - N/A
    - Colour as intended
      - I had a text panel that I forgot about it, replaced with another panel, and changed that
- Window title colour
  - 15/09/2023
  - 5 tests (5 failed)
  - Window title should be dark to match the program
    - Window title bar stayed white
  
