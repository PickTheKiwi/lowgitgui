/**
 * Classname: Main
 * Author: Sebastian White
 * Created: 2023-09-12
 * Last Modified: 2023-09-14
 * Description: The main class, this is the entry point for the program.
 */
package com.pickthekiwi;
// Project imports
import com.pickthekiwi.graphics.Home;
// Java imports
// None lol, I moved everything to graphics.Home for now

public class Main {
    public static void main( String[] args ) {
        System.out.println( "Hello World!" );
        Home.makeVisible();
    }
}
