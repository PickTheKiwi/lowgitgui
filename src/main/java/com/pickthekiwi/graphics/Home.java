/**
 * Classname: Home
 * Author: Sebastian White
 * Created: 2023-09-12
 * Last Modified: 2023-09-14
 * Description: The main GUI class, this is where all updates for the main window will be made.
 */
package com.pickthekiwi.graphics;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Home {

    /*
     * This entire class is a mess, I'll fix this later, for now it's just here to get the window up and running
     * As well as getting the layout setup.
     *
     * While I do understand Java swing, I haven't used it much, so please bear with my painful code.
     */

    private static final JFrame FRAME = new JFrame("LowGitGUI");
    private static final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

    static { // Runs when the class is loaded
        //Creating the Frame
        FRAME.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /*
         * This is for the entire window, I want the inner window to be 800x600,
         * however I'm only taking the Windows 10 window size into account.
         * I'm not sure how to set it for the content size.
         * Also, turns out Java it a bit weird with windows screen scaling, I'm assuming 125% is the default.
         * This means that the window is actually roughly 1000x750, even though it says 800x600.
         */
        FRAME.setSize(814, 637);
        // Make sure the window in centered on the screen
        // (This was implemented to attempt to fix something, but it doesn't seem to do anything, not doing to remove it though)
        FRAME.setLocation(screen.width / 2 - FRAME.getSize().width / 2, screen.height / 2 - FRAME.getSize().height / 2);
        // This helped me
        System.out.println(screen.width + " <- Screen width | Frame inner width -> " + FRAME.getSize().width);

        // Menu bar, similar to the menus you see in a lot of programs
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("FILE");
        JMenu helpMenu = new JMenu("Help");
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        // Selection in fileMenu
        JMenuItem openSelect = new JMenuItem("Open");
        fileMenu.add(openSelect);

        // Create a panel and objects to add to it
        // This panel in the future will take input and run commands on the user's behalf, assuming I remember how to do that
        JPanel textPanel = new JPanel(); // Needed to add everything below
        JLabel textLabel = new JLabel("Enter Command");
        JTextField textField = new JTextField(65); // (I was lied to when told about this, ignore my previous comment) This is the size of the text field, bit not the amount of characters
        // WHY DOESN'T THIS WORK, THIS IS SUPPOSED TO ADD PADDING TO THE TEXT FIELD
        textField.setMargin(new Insets(0, 5, 0, 0));

        JButton sendButton = new JButton("⏵");
        sendButton.setPreferredSize(new Dimension(20, 19)); // Change size of button
        sendButton.setMargin(new Insets(0, 0, 0, 0)); // Remove padding from button, so text shows up (Rather than "...")

        // Add the objects to the panel
        textPanel.add(textLabel);
        textPanel.add(textField);
        textPanel.add(sendButton);

        // Blank box to be drawn on, this will have all the graphics drawn on it
        JPanel mainPanel = new JPanel();
        mainPanel.setBackground(Color.GRAY);


        // Change some of the colours to be better on the eyes, darker "theme"
        textPanel.setBackground(new Color(66, 66, 66)); // Couldn't be bothered converting to hex, so I just used RGB
        textLabel.setForeground(Color.LIGHT_GRAY); // I need to be able to read the text, however white was too bright
        textField.setBackground(new Color(88, 88, 88)); // Slightly lighter than the panel, so it stands out
        textField.setBorder(new LineBorder(Color.DARK_GRAY));
        textField.setForeground(Color.LIGHT_GRAY);
        sendButton.setBackground(new Color(88, 88, 88));
        sendButton.setForeground(Color.LIGHT_GRAY);

        // Adding Components to the frame.
        FRAME.getContentPane().add(BorderLayout.SOUTH, textPanel);
        FRAME.getContentPane().add(BorderLayout.NORTH, menuBar);
        FRAME.getContentPane().add(BorderLayout.CENTER, mainPanel);

        // This is a listener for the window, it will print the size of the window when it's resized
        FRAME.getRootPane().addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent e) {
                // get the current size of the frame
                Dimension dim = e.getComponent().getSize();
                System.out.println("Width: " + dim.getWidth() + " Height: " + dim.getHeight());
            }
        });
    }

    public static void makeVisible() {
        FRAME.setVisible(true);
    }
}
