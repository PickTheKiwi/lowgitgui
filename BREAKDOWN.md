# Breakdown of project
This is a breakdown of the project, and how it works.
This is a bullet point list of everything I plan to do, and what I have done.
You will also find this in kanban/table forms in the [LowGitGUI project](https://github.com/users/PickTheKiwi/projects/5) I have set up.

### Base idea: have a GUI that allows the user to interact with [Git](https://git-scm.com/downloads)

Breakdown:
- Handling the main GUI
  - When first opened 
    - Open a window
    - Parse git status
    - Draw all starting information
- Handling the commands
  - Preset commands
    - Run command
    - Parse command into expected string, dropping unnecessary information
    - Return information to where it was called from
  - User input commands
    - Attempt to run command
    - Relay output to user
- Parsing command outputs
  - Get command output
  - Search for strings based off the wanted information
    - For git status look for terms such as "renamed" "modified" or "deleted"
- Pop ups (Confirmations, responses)
  - Draw information given to the object
  - Set the window to be visible
- File watching
  - Check if a file is changed, added, or removed
    - Open a listener for fie changes
    - Mark a variable to have the git status checked
  - If the main GUI is in focus, update the git status

### Note:
This may be (But is unlikely to me) more broken down in the kanban/table (linked above), I suggest taking a look at that.